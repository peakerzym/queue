import { Injectable } from "@nestjs/common";
import { InjectModel } from "nestjs-typegoose";
import { Accounts } from "src/models/account.model";
import { ReturnModelType } from "@typegoose/typegoose";
import * as mongoose from 'mongoose';

import { Configs } from "src/models/config.model";
import { Tickets } from "src/models/ticket.model";
import { InjectConnection } from "@nestjs/mongoose";
import { Log } from "src/models/log.model";
@Injectable()
export class RegisterService {

  constructor(
    @InjectModel(Accounts) 
    private readonly accountsModel: ReturnModelType<typeof Accounts>,
    @InjectModel(Configs) 
    private readonly configsModel: ReturnModelType<typeof Configs>,
    @InjectModel(Tickets) 
    private readonly ticketsModel: ReturnModelType<typeof Tickets>,
    @InjectModel(Log) 
    private readonly logModel: ReturnModelType<typeof Log>,
    @InjectConnection() 
    private readonly connection: mongoose.Connection,
  ){}

  async register (firstname:string, lastname:string, eventid:string){
    
    const dataMax = await this.configsModel.findOne({
      eventid:eventid
    })

    const num = await this.accountsModel.find({
      eventid:eventid
    }).countDocuments()

    console.log('before deley')
    try {
      await this.deley()
    } catch (error) {
      console.log(error)
    }
    
    console.log(`${num} <= ${dataMax.max}`)
    if(num < dataMax.max){

     let dataCreate = {
        firstname,
        lastname,
        eventid
      }
  
      return await this.accountsModel.create(dataCreate)
    } else {
      return `register limit ${dataMax.max}`
    }

  }

  async register2 (cId, firstname:string, lastname:string, eventid:string){
    
    //const dataTickets = await this.ticketsModel.findOne({status:"empty"})

    let dataCreate = {
      firstname,
      lastname,
      eventid,
      customer_id:cId
    }
    await this.logModel.create(dataCreate)


    /*const dataMax = await this.configsModel.findOne({
      eventid:eventid
    })

    const num = await this.accountsModel.find({
      eventid:eventid
    }).countDocuments()*/

    
    
    //if(num < dataMax.max){
      
      const res = await this.ticketsModel.findOneAndUpdate({
        status:"empty",
        customer_id:{$ne:null}
      },{
        status:"booking",
        customer_id:cId
      })

      console.log('before deley')
      try {
        await this.deley()
      } catch (error) {
        console.log(error)
      }

      if(res !== null){

        /*await this.ticketsModel.findOneAndUpdate({
          _id:res._id,
        },{
          account_id:dataRes._id
        })*/

        return await this.accountsModel.create(dataCreate)

       } else {

        return { message:'limit'}
       }
    /*} else {
      return `register limit `
    }*/
   
  }

  async register3 (firstname:string, lastname:string, eventid:string){

    const transactionSession = await this.connection.startSession();
    transactionSession.startTransaction();

    try {
      console.log('start Transaction 3')
      const dataMax = await this.configsModel.findOne({
        eventid:eventid
      })
  
      const num = await this.accountsModel.find({
        eventid:eventid
      }).countDocuments()
  
      console.log('before deley')
      try {
        await this.deley()
      } catch (error) {
        
      }

      console.log(`${num} <= ${dataMax.max}`)
      let res 
      if(num < dataMax.max){
      
        const dataCreate = {
          firstname,
          lastname,
          eventid
        }
        res = await this.accountsModel.create([dataCreate], {transactionSession})
      }else{
        res = `register limit ${dataMax.max}`
      }
      await transactionSession.commitTransaction()

      return res

    } catch (error) {
      await transactionSession.abortTransaction()
    } finally {
      transactionSession.endSession()
    }

  }


  async deley(){
    return new Promise((reject,resolve) => {
      setTimeout( () => {
        resolve('finished deley')
      }, 3000 )
    })
  }

  async deleteAllRegister (){
    return await this.accountsModel.deleteMany({});
  }

  async createTicket (eventid:string){
    const dataMax = await this.configsModel.findOne({
      eventid:eventid
    })

    let dataCreate = {
      account_id:null,
      status:"empty",
      eventid,
      customer_id:""
    }

    for (let i = 0 ; i < dataMax.max; ++i){
      await this.ticketsModel.create(dataCreate)
    }

  }

  async deleteTicket(eventid:string){
    return await this.ticketsModel.deleteMany({eventid:eventid});
  }
}
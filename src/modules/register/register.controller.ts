import { Job, Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { Controller, Get, Param, Post } from '@nestjs/common';
import { RegisterService } from './register.service';
import * as Queue2 from 'bull';

@Controller('queues')
export class RegisterController {
  constructor(
    @InjectQueue('register') private readonly registerQueue: Queue,
    private readonly registerService: RegisterService
  ) {}

  @Get('register2/:name/:id')
  async ceateQueues(@Param() param:{id:string, name: string}) {
    
    const queue = new Queue2(
      param.name, 
      'redis://127.0.0.1:8879',
      {
        limiter: {
          max: 2,
          duration: 5000
        }
      },
      /*{ lifo: true },
      { delay: 3000 }*/
    );
    
    const res = await queue.add(`reg-${param.name}`,
      {
        eventid:param.id,
        firstname:'suttipong', 
        lastname: 'buahom'
      }
    )

    queue.process(`reg-${param.name}`, async (job:Job, done) => {
      
      await this.registerService.register2(job.id , job.data.firstname+job.id, job.data.lastname, job.data.eventid)
      .then((data)=>{
        done(null, data)
      }).catch((err)=>{
        done(new Error(err))
      })

      /*return await new Promise((resolve, reject) => {
        try {
          let data = this.registerService.register(job.data.firstname+job.id, job.data.lastname, job.data.eventid)
          resolve(data)
          done(null, data)
        } catch (error) {
          reject(error);
        }
      })*/
      
    
    });
    
    queue.on('waiting', function (jobId) {
      console.log(`Waiting ${jobId}`);
    });

    queue.on('active', function (job, jobPromise) {
      console.log(`start ${job.name} ${job.id}`);
    })
    queue.on('progress', function (job, progress) {
      console.log(`Progress ${job.id} ${progress}`);
    })

    queue.on('failed', function (job, err) {
      console.log(`failed ${job.id} ${err}`);
    })

    queue.on('completed', (job, result) => {
      console.log(`Job ${job.name} ${job.id} completed with result ${result}`);
    })

    return await res.finished()

  }

  @Get('register3/:name/:id')
  async register3(@Param() param:{id:string, name: string}) {
    
    const queue = new Queue2(
      param.name, 
      'redis://127.0.0.1:8879',
      {
        limiter: {
          max: 100,
          duration: 5000
        }
      },
      /*{ lifo: true },
      { delay: 3000 }*/
    );

    const res = await queue.add(`reg-${param.name}`,
      {
        eventid:param.id,
        firstname:'suttipong', 
        lastname: 'buahom'
      }
    )

    queue.process(`reg-${param.name}`, async (job:Job, done) => {
      
      await this.registerService.register3(job.data.firstname+job.id, job.data.lastname, job.data.eventid)
      .then((data)=>{
        done(null, data)
      }).catch((err)=>{
        done(new Error(err))
      })

    
    });
    
    queue.on('waiting', function (jobId) {
      console.log(`Waiting ${jobId}`);
    });

    queue.on('active', function (job, jobPromise) {
      console.log(`start ${job.name} ${job.id}`);
    })
    queue.on('progress', function (job, progress) {
      console.log(`Progress ${job.id} ${progress}`);
    })

    queue.on('failed', function (job, err) {
      console.log(`failed ${job.id} ${err}`);
    })

    queue.on('completed', (job, result) => {
      console.log(`Job ${job.name} ${job.id} completed with result ${result}`);
    })

    return await res.finished()

  }

  @Get('register/:id')
  async register(@Param() param:{id: string}) {
      
    let res = await this.registerQueue.add('register-job', 
    {
      eventid:param.id,
      firstname:'suttipong', 
      lastname: 'buahom'
    },
    { 
      attempts: 3,
      //timeout:10000,
      backoff: {
        type: 'jitter',
      }
    })

   


    /*let waiting = await this.audioQueue.getWaiting();
    waiting.forEach(function (message) {
      console.log("waiting id is " + message.id +':'+ JSON.stringify(message.data));
    });*/

    return await res.finished()
  }

  

  @Get('register/delete/all')
  async deleteAll() {
      
    try {
      return await this.registerService.deleteAllRegister()
    } catch (e) {
      throw e;
    }
  }

  @Get('create/ticket/:id')
  async createTicket(@Param() param:{id: string}) {
    try {
      return await this.registerService.createTicket(param.id)
    } catch (e) {
      throw e;
    }
  }

  @Get('delete/ticket/:id')
  async deleteTicket(@Param() param:{id: string}) {
      
    try {
      return await this.registerService.deleteTicket(param.id)
    } catch (e) {
      throw e;
    }
  }
}
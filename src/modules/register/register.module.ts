import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { RegisterController } from './register.controller';
import { RegisterConsumer } from './register.processor';
import { RegisterService } from './register.service';
import { TypegooseModule } from 'nestjs-typegoose';
import { Accounts } from 'src/models/account.model';
import { Configs } from 'src/models/config.model';
import { Tickets } from 'src/models/ticket.model';
import { MongooseModule } from '@nestjs/mongoose';
import { Log } from 'src/models/log.model';

@Module({
  imports: [
    BullModule.registerQueue(
      {
        name: 'register',
      }
    ),
    MongooseModule.forRoot('mongodb+srv://peakerzym:tGBiOAkRGANCbVf0@peakerzym.ri1wmmb.mongodb.net/queues'),
    TypegooseModule.forFeature([
      Accounts,
      Configs,
      Tickets,
      Log
    ]),
    
  ],

  controllers: [RegisterController],
  providers: [RegisterService, RegisterConsumer],
})
export class RegisterModule {}
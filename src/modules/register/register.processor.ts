import { Job } from 'bull';
import { OnQueueActive, OnQueueError,OnQueueCompleted, OnQueueFailed, OnQueueWaiting, Process, Processor, OnQueueProgress, OnGlobalQueueCompleted } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { RegisterService } from './register.service';

@Processor('register')
export class RegisterConsumer {
  private readonly logger = new Logger(RegisterConsumer.name);

  constructor(
    private readonly registerService: RegisterService
  ) {}

  @Process('register-job')
  async handleTranscode(job: Job) {
    
    /*return await new Promise((resolve, reject) => {
      try {
        
        resolve(this.registerService.register(job.data.firstname+job.id, job.data.lastname, job.data.eventid))
      } catch (error) {
        reject(error);
      }
    })*/
    return await this.registerService.register(job.data.firstname+job.id, job.data.lastname, job.data.eventid)
  }
  

  @OnQueueWaiting()
  onWaiting(jobId: string) {
    this.logger.debug(`- OnQueueWaiting : ${jobId}`);
    return 'OnQueueWaiting'
  }

  @OnQueueFailed()
  onFailed(job: Job, err: Error){
    this.logger.debug(`- OnQueueFailed : ${job.id}`, err);
  }

  @OnQueueError()
  onError(error: Error){
    this.logger.debug(`- OnQueueError : ${error}`);
  }

  @OnQueueActive()
  onActive(job: Job<unknown>) {
    this.logger.debug(`- Starting job ${job.id} : ${JSON.stringify(job.data)}`);
  }

  
  @OnQueueCompleted()
  onCompleted(job: Job<unknown>, result: any) {
    this.logger.debug(`- Job ${job.id} has been finished`, ' -> result: ', result);
  }

  @OnGlobalQueueCompleted()
  async onGlobalCompleted(jobId: number, result: any) {
  }

  @OnQueueProgress()
  onProgress(job: Job, progress: number) {
    //this.logger.debug(`- OnQueueProgress Job ${job.id} -${progress}`);
  }
}


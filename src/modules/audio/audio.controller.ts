import { Queue } from 'bull';
import { InjectQueue } from '@nestjs/bull';
import { Controller, Get, Post } from '@nestjs/common';

@Controller('v1/file')
export class AudioController {
  constructor(
    @InjectQueue('audio') private readonly audioQueue: Queue,
  ) {}

  @Get()
  async transcode() {

      let id=1
      
      let re = await this.audioQueue.add('audio-job', 
      {id:id,file: `audio${id}.mp3`},
      { 
        attempts: 3,
        backoff: {
          type: 'jitter',
        }
      })


    /*let waiting = await this.audioQueue.getWaiting();
    waiting.forEach(function (message) {
      console.log("waiting id is " + message.id +':'+ JSON.stringify(message.data));
    });*/

    return  await re.finished()
  }
}
import { Job } from 'bull';
import { OnQueueActive, OnQueueError,OnQueueCompleted, OnQueueFailed, OnQueueWaiting, Process, Processor, OnQueueProgress, OnGlobalQueueCompleted } from '@nestjs/bull';
import { Logger } from '@nestjs/common';

@Processor('audio')
export class AudioConsumer {
  private readonly logger = new Logger(AudioConsumer.name);
  @Process('audio-job')
  async handleTranscode(job: Job) {

    /*let progress = 0;
    for (let i = 0; i < 100; i++) {
      progress += 1;
      await job.progress(progress);
    }*/
    
    /*await new Promise((resolve, reject) => {
      try {
        /*setTimeout(
          () => {
            resolve('Data processed');
          },
          1000 + Math.floor(Math.random() * 2000),
        );*/
        /*resolve('Data processed')
      } catch (error) {
        reject(error);
      }
    })*/

    /*await new Promise((resolve, reject) => {
      try {
        
        resolve('Data processed');
        //job.moveToCompleted('done555', true)
         
      } catch (error) {
        reject(error);
      }
    })*/

    return 'test';

    /*if(job.data.id===5){
      console.log(fail, job.data);
      job.moveToFailed({ message: 'fail 555' })
    } 
    else {
      console.log(job.data);
      job.moveToCompleted('done555', true)
    }*/
    
  }

  @OnQueueWaiting()
  onWaiting(jobId: string) {
    this.logger.debug(`- OnQueueWaiting : ${jobId}`);
    return 'OnQueueWaiting'
  }

  @OnQueueFailed()
  onFailed(job: Job, err: Error){
    this.logger.debug(`- OnQueueFailed : ${job.id}`, err);
  }

  @OnQueueError()
  onError(error: Error){
    this.logger.debug(`- OnQueueError : ${error}`);
  }

  @OnQueueActive()
  onActive(job: Job<unknown>) {
    this.logger.debug(`- Starting job ${job.id} : ${job.data['id']}`);
  }

  
  @OnQueueCompleted()
  onCompleted(job: Job<unknown>, result: any) {
    this.logger.debug(`- Job ${job.id} has been finished`, ' -> result: ', result);
  }

  @OnGlobalQueueCompleted()
  async onGlobalCompleted(jobId: number, result: any) {
  }

  @OnQueueProgress()
  onProgress(job: Job, progress: number) {
    //this.logger.debug(`- OnQueueProgress Job ${job.id} -${progress}`);
  }
}


import { modelOptions, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

export class Log extends TimeStamps {

  @prop({ required: true })
  firstname:string
  
  @prop({ required: true })
  lastname: string
  
  @prop({ required: true })
  eventid: string

  @prop({ required: true})
  customer_id:string
  
}
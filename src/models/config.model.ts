import { modelOptions, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

export class Configs extends TimeStamps {

  @prop({ required: true })
  max:number
  
}
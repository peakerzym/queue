import { Ref, modelOptions, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";
import { ObjectId } from "mongoose";
import { Accounts } from "./account.model";

@modelOptions({ 
  schemaOptions: { 
    timestamps: true,
    versionKey: false
  }
})

export class Tickets extends TimeStamps {
  
  @prop({ ref: () => Accounts })
  account_id:Ref<Accounts>

  @prop({ required: false})
  customer_id:string

  @prop({ required: true })
  status:string

  @prop({ required: true })
  eventid: string
  
}
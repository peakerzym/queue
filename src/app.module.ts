import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BullModule } from '@nestjs/bull';
import { AudioModule } from './modules/audio/audio.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { Accounts } from './models/account.model';
import { Configs } from './models/config.model';
import { RegisterModule } from './modules/register/register.module';

@Module({
  imports: [
    TypegooseModule.forRootAsync({
      imports: [],
      useFactory: async () => ({
        uri: 'mongodb+srv://peakerzym:tGBiOAkRGANCbVf0@peakerzym.ri1wmmb.mongodb.net/queues',
      }),
      inject: []
    }),
    BullModule.forRootAsync({
      useFactory: () => ({
        redis: {
          host: 'localhost',
          port: 8879,
        },
        limiter: {
          max: 100,
          duration: 5000,
          bounceBack: false
        },
        settings: {
          backoffStrategies: {
            jitter: function (attemptsMade, err) {
              return 5000;
            }
          }
        }
      }),
    }),
    AudioModule,
    RegisterModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
